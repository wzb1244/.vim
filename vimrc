let mapleader=" "
syntax on
set nocompatible
filetype on
filetype indent on
filetype plugin on
filetype plugin indent on
set mouse=a
set encoding=utf-8
let &t_ut=''
set list
set listchars=tab:▸\ ,trail:▫
set scrolloff=5
set tw=0
set indentexpr=
set backspace=indent,eol,start
set foldmethod=indent
set foldlevel=99
set cursorline
let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_SR = "\<Esc>]50;CursorShape=2\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"
set laststatus=2
set autochdir
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
map sl :set splitright<CR>:vsplit<CR>
map sh :set nosplitright<CR>:vsplit<CR>
map sk :set nosplitbelow<CR>:split<CR>
map sj :set splitbelow<CR>:split<CR>
map te :tabe<CR>
map tp :tabp<CR>
map tn :tabn<CR>
map tmp :-tabmove<CR>
map tmn :+tabmove<CR>
map ` ~
map <LEADER>h <C-w>h
map <LEADER>h <C-w>h
map <LEADER>j <C-w>j
map <LEADER>k <C-w>k
map <LEADER>l <C-w>l
map th <C-w>h
map tj <C-w>j
map tk <C-w>k
map tl <C-w>l
map tr <C-r>
map tv <C-v>
map tc :nohl<CR><ESC>
map <up> :res +5<CR>
map <down> :res -5<CR>
map <left> :vertical resize-5<CR>
map <right> :vertical resize+5<CR>
noremap n nzz
noremap N Nzz
noremap j jzz
noremap k kzz
inoremap ' ''<ESC>i
inoremap " ""<ESC>i
inoremap [ []<ESC>i
inoremap ( ()<ESC>i
inoremap { {}<ESC>i
inoremap < <><ESC>i
inoremap ` ``<ESC>i

func SkipPair()
    if getline('.')[col('.') - 1] == '>' || getline('.')[col('.') - 1] == '}' || getline('.')[col('.') - 1] == ')' || getline('.')[col('.') - 1] == ']' || getline('.')[col('.') - 1] == '"' || getline('.')[col('.') - 1] == "'" || getline('.')[col('.') - 1] == "`"

        return "\<ESC>la"
    else
        return "\t"
    endif
endfunc
inoremap <TAB> <c-r>=SkipPair()<CR>

set number
set relativenumber
set autoindent
set smartindent
set smarttab
set expandtab
set hlsearch
set incsearch
set ignorecase
set smartcase
set wrap
set wildmenu
set tabstop=4
set shiftwidth=4

set complete=.,w,b,u,t
autocmd FileType * set omnifunc=syntaxcomplete#Complete
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType java set omnifunc=javacomplete#Complete
autocmd FileType c set omnifunc=ccomplete
let g:rubycomplete_buffer_loading = 1
let g:rubycomplete_classes_in_global = 1

call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline'
" File navigation
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'Xuyuanp/nerdtree-git-plugin'

" Other visual enhancement
Plug 'nathanaelkane/vim-indent-guides'
Plug 'itchyny/vim-cursorword'

" Git
Plug 'rhysd/conflict-marker.vim'
Plug 'tpope/vim-fugitive'
Plug 'mhinz/vim-signify'

" Python
Plug 'vim-scripts/indentpython.vim'


" Makdown
Plug 'dhruvasagar/vim-table-mode', { 'on': 'TableModeToggle' }

" Bookmarks
Plug 'kshenoy/vim-signature'

" Other useful utilities
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'

" Dependencies
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'kana/vim-textobj-user'
Plug 'fadein/vim-FIGlet'

call plug#end()


" NERDTree
map tt :NERDTreeToggle<CR>
let NERDTreeMapOpenExpl = ""
let NERDTreeMapMapUpdir = ""
let NERDTreeMapMapUpdirKeepOpen = "l"
let NERDTreeMapMapOpenSplit = ""
let NERDTreeOpenVSplit = ""
let NERDTreeMapActivateNode = "i"
let NERDTreeMapOpenInTab = "o"
let NERDTreeMapPreview = ""
let NERDTreeMapCloseDir = "c"
let NERDTreeMapChangeRoot = "r"

" NERDTreeGit
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }


" vim indent guide
let g:indent_guides_guide_size = 1
let g:indent_guides_start_level = 2
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_color_change_percent = 1
silent! unmap tig
autocmd WinEnter * silent! unmap tig
set nolist

" python syntax
let g:python_highlight_all = 1

" vim table mode
map ttm :TableModeToggle<CR>
